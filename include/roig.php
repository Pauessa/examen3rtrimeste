<?php
	session_start();
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title>Roig</title>
		<link  rel="stylesheet" href="../css/estilsProcessa.css" />
		<link  rel="stylesheet" href="../css/estilsRoig.css" />
	</head>
	<body>
		<div id="wrapper">
			<header id="cap">
				<img src="../img/logophp.png" alt="logo PHP" class="fotocap" />
				<h1>Examen PHP Exercici 01 :: Roig</h1>
				<img src="../img/logophp.png" alt="logo PHP" class="fotocap" />
			</header>
			<section id="contingut">


				<article class="exercici">
					<?php
								$nom=$_SESSION["nom"];
								$desc=$_SESSION["desc"];

								echo strtoupper($nom)."<br>";
								echo $desc;


					  ?>

				</article>

			</section>
			<footer id="peu">
				<?php
					include 'peu.php';
				?>
			</footer>
		</div>
	</body>
</html>
