pi:Un pi és un arbre del gènere Pinus, de la mateixa família que els avets, els cedres i les picees entre altres:
roure:El terme roure pot ser usat per a referir-se a moltes espècies d'arbres del gènere Quercus, nadiu de l'hemisferi nord:
salze:El salze (Salix L.) és un gènere de plantes amb flor de la família de les salicàcies:
carrasca:La carrasca (Quercus rotundifolia o Quercus ilex rotundifolia) és un arbre de la família de les fagàcies, molt semblant a l'alzina (Quercus ilex):
faig:El faig (Fagus sylvatica), és un arbre gran i corpulent del gènere Fagus i de la família Fagaceae:
